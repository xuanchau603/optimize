using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Board : MonoBehaviour
{
    [SerializeField] CanvasGroup   m_board;
    [SerializeField] TMP_Text      m_text;
    [SerializeField] Button        m_btnShow;
    [SerializeField] Button        m_btnHide;
    [SerializeField] RectTransform m_rectTransform;
    [SerializeField] Vector2       m_targetPos;
    [SerializeField] TMP_Text      m_floatText;
    Vector3                        m_initPos;

    void Start()
    {
        m_initPos = m_rectTransform.localPosition;
    }


    public void Hide()
    {
        m_rectTransform.anchoredPosition = m_initPos;
        gameObject.SetActive(false);
    }

    void OnEnable()
    {
        m_text.gameObject.SetActive(false);
        m_board.DOFade(1, 2);
        m_rectTransform.DOAnchorPos(m_targetPos, 2).SetEase(Ease.Linear).OnComplete(TextButtonAnim);
    }

    void TextButtonAnim()
    {
        m_text.gameObject.SetActive(true);
        var text = m_text.text;
        var txt  = "";
        DOTween.To(() => txt, value => txt = value, text, 2).OnUpdate(() =>
        {
            m_text.text = txt;
        }).OnComplete(() =>
        {
            m_text.DOColor(Color.yellow, 10);
            DOVirtual.Float(0, 100,  3,    (value) => m_floatText.text = value.ToString());
            DOVirtual.Float(1, 1.2f, 0.5f, (x) => { m_btnHide.transform.localScale = Vector3.one * x; }).SetLoops(-1, LoopType.Yoyo);
        });
    }
}