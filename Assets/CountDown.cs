using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;

public class CountDown : MonoBehaviour
{
    [SerializeField] float      m_Time;
    [SerializeField] float      m_duration;
    [SerializeField] GameObject m_txtCountDown;
    TMP_Text                    m_txtCount;

    ReactiveProperty<int> m_number = new ReactiveProperty<int>(0);
    void Start()
    {
        m_txtCount = m_txtCountDown.GetComponent<TMP_Text>();
        
        
        //Update text when m_number change
        m_number.Subscribe((value) =>
        {
            m_txtCount.text = value.ToString();
        });
    }

    void SCountDown()
    {
        m_Time -= m_duration;
        if (m_Time >= 0)
        {
             m_txtCountDown.GetComponent<TMP_Text>().text = m_Time.ToString();
            //m_txtCount.text = m_Time.ToString();
        }
        else
        {
            Debug.Log("End time!");
        }
    }
    

    public void BtnIncreaseOnClick()
    {
        //InvokeRepeating(nameof(SCountDown), 0, m_duration);
        m_number.Value++;
    }
}
