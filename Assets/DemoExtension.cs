using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public class MaxLengthAttribute : PropertyAttribute
{
    public int maxLength;

    public MaxLengthAttribute(int maxLength)
    {
        this.maxLength = maxLength;
    }
}


public class DemoExtension : MonoBehaviour
{
    //Show warning label
    [MaxLength(10)] public int    m_playerAge;
    
    //If lenght > 5 replace lenght to 5
    [MaxLength(5)]  public string m_playerName;

    [Tooltip("Speed of player")]
    public int Speed;
    
    
    
    
    void Start()
    {
        transform.SetPosX(1);
        
        Debug.Log(transform.GetPosX());
        
        //New
        Animal animal = new Animal();

        //Reflection
        Type type    = typeof(Animal);
        var  animal1 = Activator.CreateInstance(type);

        
        Debug.Log(animal);
        Debug.Log(animal1);

        
        //animal.Run();

        MethodInfo runMethod = type.GetMethod("Run");

        var speedAnimal = type.GetProperties();
        
        Debug.Log(speedAnimal);
        
        runMethod?.Invoke(animal1, null);
    }
}

public static class Extensions
{
    public static void SetPosX(this Transform transform, float newX)
    {
        Vector3 newPos = new Vector3(newX, transform.position.y, transform.position.z);
        transform.position = newPos;
    }
    
    public static float GetPosX(this Transform transform)
    {
        return transform.position.x;
    }
    
    public static void SetPosY(this Transform transform, float newY)
    {
        Vector3 newPos = new Vector3(transform.position.x, newY, transform.position.z);
        transform.position = newPos;
    }
    
    public static void SetPosZ(this Transform transform, float newZ)
    {
        Vector3 newPos = new Vector3(transform.position.x, transform.position.y, newZ);
        transform.position = newPos;
    }
}

[CustomPropertyDrawer(typeof(MaxLengthAttribute))]
public class MaxLengthAttributeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType == SerializedPropertyType.String)
        {
            MaxLengthAttribute maxLengthAttribute = attribute as MaxLengthAttribute;

            EditorGUI.BeginChangeCheck();
            string newValue = EditorGUI.TextField(position, label, property.stringValue);

            if (newValue.Length > maxLengthAttribute.maxLength)
            {
                newValue = newValue.Substring(0, maxLengthAttribute.maxLength);
            }

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = newValue;
            }
        }
        else
        {
            EditorGUI.LabelField(position, label, new GUIContent("This attribute supports string properties only."));
        }
    }
}
