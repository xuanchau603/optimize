using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DropDown : MonoBehaviour
{
    [SerializeField] TMP_Dropdown m_dropdown;
    void Start()
    {
        if (!PlayerPrefs.HasKey("Language"))
        {
            PlayerPrefs.SetInt("Language", 0);
        }
        InitLanguage();
    }
    void InitLanguage()
    {
        int index = PlayerPrefs.GetInt("Language");
        m_dropdown.value                        = index;
        LanguageManager.instance.Language.Value = GetLanguageFromIndex(index);
        m_dropdown.onValueChanged.AddListener((indexDropdown) =>
        {
            PlayerPrefs.SetInt("Language", indexDropdown);
            LanguageManager.instance.Language.Value = GetLanguageFromIndex(indexDropdown);
        });
    }

    string GetLanguageFromIndex(int index)
    {
        switch (index)
        {
            case 0:
                return "VN"; 
            case 1:
                return "EN";
            case 2:
                return "JP";
            default:
                return "VN";
        }
    }
}
