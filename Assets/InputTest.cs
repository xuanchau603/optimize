using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InputTest : MonoBehaviour
{
    [SerializeField] TMP_InputField m_inputField;
    [SerializeField] Button         m_button;

    Regex emailRegex = new Regex(@"^\w+@gearinc+\.com|vn$", RegexOptions.None);
    
    void Start()
    {
        m_inputField.onValueChanged.AddListener((value) =>
        {
            Debug.Log(emailRegex.IsMatch(value));
            m_button.interactable = emailRegex.IsMatch(value);
        });
    }
}
