using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;


public enum KeyText
{
    WelcomeText,
    StartButtonText,
    OptionsButtonText
}

public class Language
{
    public string Key;
    public string VN;
    public string EN;
    public string JP;
}

public class LanguageManager : MonoBehaviour
{
    public static LanguageManager   instance;
    Dictionary<string, Language>    languageData = new Dictionary<string, Language>();
    public ReactiveProperty<string> Language     = new ReactiveProperty<string>();
    List<Language>                  m_languages  = new List<Language>();
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);

        // transform.position.x = 3;
        // transform.position.y = 3;
        // transform.position.z = 3;
        
        transform.position = Vector3.one * 3;

        //transform.PosX = 3;
    }

    public async UniTask LoadLanguageData(string language)
    {
        string apiUrl = @"https://sheetdb.io/api/v1/d7ha7ez2etgei";
        using (UnityWebRequest webRequest = UnityWebRequest.Get(apiUrl))
        {
            await webRequest.SendWebRequest();
            if (webRequest.result == UnityWebRequest.Result.ConnectionError || 
                webRequest.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError("Error: " + webRequest.error);
            }
            else
            {
                Debug.Log("API Response: " + webRequest.downloadHandler.text);
                string jsonText = webRequest.downloadHandler.text;
                m_languages = JsonConvert.DeserializeObject<List<Language>>(jsonText);
                foreach (var item in m_languages)
                {
                    languageData[item.Key] = item;
                }
                Debug.Log(languageData);
            }
        }
    }

    public string GetText(string key)
    {
        switch (Language.Value)
        {
            case "VN":
                return languageData[key].VN;
            case "EN":
                return languageData[key].EN;
            case "JP":
                return languageData[key].JP;
            default: return languageData[key].VN;
        }
    }
}
