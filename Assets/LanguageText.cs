using TMPro;
using UniRx;
using UnityEngine;

public class LanguageText : MonoBehaviour
{
    [SerializeField] TMP_Text m_text;
    [SerializeField] string  m_keyText;
    void Start()
    {
        if (!m_text)
        {
            m_text = GetComponent<TMP_Text>();
        }
        LanguageManager.instance.Language.Subscribe( async (language) =>
        {
            await LanguageManager.instance.LoadLanguageData(language);
            m_text.text = LanguageManager.instance.GetText(m_keyText);
        });
    }
}
