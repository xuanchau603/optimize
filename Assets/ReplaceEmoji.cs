using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ReplaceEmoji : MonoBehaviour
{
    [SerializeField] TMP_Text m_text;
    List<string> m_emojiString = new List<string>{":a",":b",":c",":d",":e"};

    void Awake()
    {
        m_text = GetComponent<TMP_Text>();
        ReplaceEmojiText();
    }

    void ReplaceEmojiText()
    {
        string originalText = m_text.text;
        foreach (var item in m_emojiString)
        {
            var replace = originalText.Replace(item, $"<sprite name=\"{item}\">");
            originalText = replace;
        }
        m_text.text = originalText;
        Debug.Log(m_text.text);
    }
    
}
