using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioType
{
    Hit = 0,
    Punch = 1,
    Hurt = 2,
    BackGround = 3
}
[Serializable]
public class Audio
{
    public AudioClip AudioClip;
    public AudioType AudioType;
}

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    
    [SerializeField] List<Audio> Audios = new List<Audio>();
    [SerializeField] AudioSource m_audioSource;
    [SerializeField] AudioSource m_audioSourceBackground;

    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
        PlayBackGroundAudio();
    }

    AudioClip GetAudioByType(AudioType type)
    {
        for (int i = 0; i < Audios.Count; i++)
        {
            if (type == Audios[i].AudioType)
            {
                return Audios[i].AudioClip;
            }
        }
        return null;
    }

    public void PlayAudio(AudioType type)
    {
        var clip = GetAudioByType(type);
        m_audioSource.PlayOneShot(clip);
    }

    void PlayBackGroundAudio()
    {
        m_audioSourceBackground.Play();
        m_audioSourceBackground.loop = true;
    }
    
    public void PauseBackGroundAudio()
    {
        if (m_audioSourceBackground.isPlaying)
        {        
            m_audioSourceBackground.Pause();
        }
        else
        {
            m_audioSourceBackground.UnPause();
        }
    }
    
    
}
