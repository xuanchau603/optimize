using System;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class Animal
{
    public string Name { get; set; }
    public int Age { get; set; }

    float m_speed = 10;


    void Run()
    {
        Debug.Log("Runing");
    }
}


public class TestDoTween : MonoBehaviour
{
    [SerializeField] GameObject m_ball;
    [SerializeField] Transform  m_target;
    Tween                       m_tween;

    void Start()
    {
        
    }


    public void DoMoveOnClick()
    {
        m_tween = m_ball.transform.DOMove(m_target.position, 4).OnComplete(() =>
        {
            var sprite = m_ball.GetComponent<SpriteRenderer>();
            sprite.DOFade(0, 1);
        });;
        
    }

    public void DoMoveAndRotateOnClick()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(transform.DOMoveY(4,1));
        sequence.Append(transform.DORotate(new Vector3(0,0,180),0.2f));
    }

    public void DoJumpOnClick()
    {
        m_ball.transform.DOJump(m_target.position, 2f, 6, 2);
    }

    public void Reset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void OnDestroy()
    {
        Debug.Log("Kill");
        m_tween.Kill();
    }
}
