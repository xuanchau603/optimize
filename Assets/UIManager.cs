using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] Button m_btnHit;
    [SerializeField] Button m_btnPunch;
    [SerializeField] Button m_btnHurt;

    [Conditional("ENABLE_LOG")]
    void Start()
    {
        m_btnHit.onClick.AddListener(()=> SoundManager.Instance.PlayAudio(AudioType.Hit));
        m_btnPunch.onClick.AddListener(()=> SoundManager.Instance.PlayAudio(AudioType.Punch));
        m_btnHurt.onClick.AddListener(()=> SoundManager.Instance.PlayAudio(AudioType.Hurt));
    }

    
}
