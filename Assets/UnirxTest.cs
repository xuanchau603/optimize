using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using Cysharp.Threading.Tasks;
using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;
using Newtonsoft.Json;
using Test.Proto.GearInc;
using TMPro;
using UnityEngine;
using UniRx;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;


[Serializable]
public class MyData
{
    public string userId;
    public string id;
    public string title;
    public string body;
}

public class UnirxTest : MonoBehaviour
{
    [SerializeField] TMP_Text m_text;
    [SerializeField] Button   m_btnCallApi;
    ReactiveProperty<int>     m_cash = new ReactiveProperty<int>();
    
    string csvPath  = "/Users/dn-plg-a0682/Documents/ChauLX-GearInc/DemoOptimize/Assets/Data/data.csv";
    string jsonPath = "/Users/dn-plg-a0682/Documents/ChauLX-GearInc/DemoOptimize/Assets/Data/data.json";

    const string m_apiUrl = "https://jsonplaceholder.typicode.com/posts";

    public UnityEvent m_myEvent;

    public UnityAction m_myAction;


    public CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();

    void Start()
    {
        Person person = new Person();
        person.Name    = "LXC";
        person.Age     = 18;
        person.Hobbies = "Eat egg";
        var data = person.ToByteArray();

        Debug.Log(data.Length);

        Person deserial = Person.Parser.ParseFrom(data);
        Debug.Log(deserial);
        var clickStream = Observable.EveryUpdate().Where(_ => Input.GetMouseButtonDown(0));
        clickStream.Buffer(clickStream.Throttle(TimeSpan.FromMilliseconds(250)))
                   .Where(xs => xs.Count >= 2)
                   .Subscribe(xs => Debug.Log(xs));

        m_cash.Subscribe(newValue =>
        {
            Debug.Log($"Value change: {newValue}");
        });

        m_btnCallApi.onClick.AddListener(async () =>
        {
            // m_myAction = Listener1;
            // m_myAction = Listener2;
            // m_myAction?.Invoke();

            //m_myEvent.AddListener(Listener1);
            // m_myEvent.AddListener(Listener2);
            // m_myEvent.Invoke();


            // List<MyData> datas = await FetchDataFromServer(CancellationTokenSource.Token);
            // Debug.Log(datas);

           

            Stopwatch stopwatch = new Stopwatch();
            
            stopwatch.Start();
            byte[]     bytesData   = await File.ReadAllBytesAsync(csvPath);
            List<Post> postsBinary = DeserializePostList(bytesData);
            stopwatch.Stop();
            Debug.Log($"Read binary data: {stopwatch.ElapsedMilliseconds}ms");

            
            stopwatch.Start();
            string     jsonData = await File.ReadAllTextAsync(jsonPath);
            List<Post> postsJson    = ConvertJsonToPostList(jsonData);
            stopwatch.Stop();
            Debug.Log($"Read json data: {stopwatch.ElapsedMilliseconds}ms");
            
            
            Debug.Log(postsBinary);
            Debug.Log(postsJson);

        });
    }

    [ContextMenu(nameof(InitData))]
    void InitData()
    {
        

        List<Post> posts = new List<Post>();
        for (int i = 0; i < 1000; i++)
        {
            Post post = new Post{Id = i,UserId = i+1,Title = i+2.ToString(),Body = i+3.ToString()};
            posts.Add(post);
        }

        
        byte[] bytesData = SerializePostList(posts);
        File.WriteAllBytes(csvPath,bytesData);

        string jsonData = ConvertPostListToJson(posts);
        File.WriteAllText(jsonPath, jsonData);
    }
    
    byte[] SerializePostList(List<Post> posts)
    {
        using (MemoryStream stream = new MemoryStream())
        {
            foreach (var post in posts)
            {
                post.WriteDelimitedTo(stream);
            }
            return stream.ToArray();
        }
    }
    
    List<Post> DeserializePostList(byte[] data)
    {
        List<Post> posts = new List<Post>();
        using (MemoryStream stream = new MemoryStream(data))
        {
            while (stream.Position < stream.Length)
            {
                Post post = Post.Parser.ParseDelimitedFrom(stream);
                posts.Add(post);
            }
        }
        return posts;
    }

    string ConvertPostListToJson(List<Post> posts)
    {
        return JsonConvert.SerializeObject(posts);
    }
    
    List<Post> ConvertJsonToPostList(string json)
    {
        return JsonConvert.DeserializeObject<List<Post>>(json);
    }


    public async UniTask FakeCallAPI()
    {
        Debug.Log("Start call api!");
        await UniTask.Delay(5000, cancellationToken: CancellationTokenSource.Token);
        Debug.Log("Finish call api");
    }

    public async void TestCancelUniTask()
    {
        FakeCallAPI();
        Destroy(gameObject, 2);
    }


    public void CancelAPICall()
    {
        Debug.Log("Cancel call API");
        CancellationTokenSource?.Cancel();
        CancellationTokenSource?.Dispose();
        CancellationTokenSource = null;
    }

    void OnDestroy()
    {
        //Cancel unitask when destroy
        CancelAPICall();
    }
}