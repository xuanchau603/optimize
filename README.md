Link Detail document: https://docs.google.com/document/d/1_qT1QyyCQkipu4sVmCdJDujJsOvy8ASQ23ZYXuixu9Q/edit?usp=sharing



Performance (Performance):




Optimize Polygons: Use fewer polygons for game objects, and remove unnecessary vertices and edges.


LOD (Level of Detail): Use LOD models to display versions of the model with varying granularity depending on the distance of the player.


Culling: Use culling to display only objects that are actually visible to the player.


Batching: Combine multiple objects with the same material to reduce the number of draw calls.
Texture Optimization:




Use Texture Compression: Choose the correct compression format for textures (e.g. DXT, ETC, ASTC) to reduce file size.


Textures Size: Use appropriate texture sizes and only use large sizes when necessary.
Code optimization:




Don't use multiple GetComponent(): To avoid redundant GetComponent() calls, it's a good idea to save references to variables when possible and reuse them.


Implement Object Pooling: Reuse pre-created objects instead of creating new ones continuously to avoid frequent memory creation/freeing.


Light Detail Limit: Adjust light granularity to reduce performance load.
Audio Optimization:




Use Audio Compression: Compress audio to reduce file size and load faster.


Customize the volume: Make sure the sound files are not too loud or small, and control the volume in the game.
UI optimization:




Use UI Image Sprite Atlas: Combine sprites into atlas to reduce memory and speed up page loading.
Physics optimization:




Use Rigidbody Interpolation



1. UniRx: (IntReactiveProperty, StringReactiveProperty, .....)
2. UniTask: Run a task, cancel task.
3. Proto buff ( syntax, build from .proto file, read data )
4. Read CSV file.
5. Text mesh pro( Tag, Icon for text, regex )
6. Layouts in unity
7. Addressable ( local )
8. Dotween( move, fade, DOTween.to, DOVitural, Kill )
Type, attribute, reflection in c#




Cache a variable








Don't cache








Only load resource current scene(Multiple scene)



Load all resource(Use prefabs)




Before pack


After pack




Audio



